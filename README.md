# Face's Story
## Chapter 1
  It had been a long day. The work for the day had been tedious, far too menial
for an animal as talented as myself. When we were finally turning to go back
home, I heard a terrible noise. As soon as we saw what was coming up the hill,
the humans tried to hurry me to the nearest shelter. HA! and HA! again. No mere
human is going to tell Face what to do. Then again, the noises that the group
were making sounded pretty scary, so I decided (on my own accord!) that I
might make my way up the hill to the keep.

  Just as I got inside the keep, the humans slammed the door so nothing and
nobody could get in or out. I made my way to the stables and greeted my cousins
with my customary, "Hee Haw!" only to be met with the indifferent blank stares
that horses and donkeys are so known for. With the pleasantries out of the way,
I found an empty stall and made a space for myself. The humans brought me some
food just as a new, more terrifying noise presented itself with an incredibly
loud boom. I saw the humans running for cover just as I saw what they were
calling a "Dragon" fly past. Unfazed, I simply laid down to wait out whatever
fuss the humans were making turned into the nothing that it always did. I soon
fell fast asleep

## Chapter 2
  I've been told that I'm a strange beast for dreaming while I sleep. I often
dream of the future, and where my life could go if I let myself live it. This
time I dreamed of my mother. She was a powerful, beautiful warhorse; a favorite
of the riders who took her into battle. Her life was long illustrious, leading
her riders to victory in almost every case. I'd never met her, but the stories
my father told of her exploits inspired a drive in me to be more than my meager,
mutated genes would suggest I should be. Every time I met my mother in my
dreams, it was bliss.

  My father was the smartest donkey in the county. An ass with a knack for
escaping even the most secure pen, he had a full stable of mares and jennies
to visit on his nightly adventures. It was on one of his early summer
escapades that he found my mother. A little less than a year later, I was born
into painful adversity. The surprise that the breeders had when they first
met me was palpable. I was immediately transferred to my father's owner since
my father was well known for his antics.

## Chapter 3
  Over the next few years, I was shuffled from owner to owner, never really
staying long enough to get to know the people. I grew to know only distaste for
the people who would take me in. Each owner would do their best to break me,
none of them trying to understand why I acted the way I did.

  Finally I was found by an understanding family who simply wanted to have me
work the fields. They worked me to the bone and never were much for
conversation, but that let me keep my thoughts to myself and ponder my place in
the world. Eventually I determined that I must go on a quest to find myself,
all that was left was to get away from the daily toil of the farm.

## Chapter 4
  I woke up when a scuffle broke out in the courtyard. A confusing band of
humans and other astonishing warriors very confusingly appearing to attack the
guard. I'd never seen such a display of power: it took nearly the entire force
to subdue them, and even then it was on their own terms that they surrendered.

  Despite the initial scuffle, when the dragon people broke through in the
middle of the night the group re-appeared. I was so enamored by their skill
that I determined that I must join them on their quest and begin my journey of
self-discovery. I had no idea how I would do it, but I felt there must be a
way.

## Chapter 5
  I made my presence known to these fantastic travelers. At first, I was afraid
they would not take me as their own, but after demonstrating my strength and
cunning, I was selected with no contest to join them. Finally, I would be able
to make my way in this world, learn about myself, and show the world what I
have to offer.

  I was so excited! Here I was, on my way with a ragtag group of travelers who
seemed to have the same thing in mind that I did. My farm life days were over!
Gone were the days spent toiling in the mud just to get some small meal of stale
oats and a cold stable. No longer would I be beaten and whipped by my masters
simply because I wasn't moving fast enough.

  An added benefit to this group was that there were no horses to be seen. Given
my questionable lineage, I'd never had any respect from any of those breeds.
Despite being clearly smarter and stronger than most (if not all) of them, they
would look down their long noses at my stiff mane and short stature and dismiss
me as a low animal, not even worthy of acknowledgement much less acceptance or
companionship.

## Chapter 6
  We were traveling south, on some sort of mission to save someone as far as I
could gather, having overheard some of the conversation that the too-loud mayor
had with my new companions. The road we were following had been trampled by a
large group, probably the dragon people that we'd encountered the previous day.
The road itself was fairly clear, although there were various rock outcroppings
that we had to wind around, breaking the monotony of the flat field we were
traveling on.

  I'd been hooked to a standard cart that was filled with many things, although
not the normal load I was used to. Rather than grain or hay which was my
expectation, this cart was outfitted for war: weapons of many kinds, food, rope,
traps, and all manner of cooking supplies. It was a fairly new cart, not yet
having fully worn-in the bearing surfaces so it still squeaked a small amount
on the left side. This trip will certainly do the trick.

  Near the end of the day, we were joined by a mysterious and confusing old man
who said his name was... Tim??? He wielded fire with such skill that I felt he
would make a great addition to our group. Soon we came upon yet another outcrop,
but this time it was different. I was told to stay back and hide off the side
of the road. The rest of the group crept forward, and over the next few minutes,
I was serenaded by the sounds of fire being set, traps being sprung, and the
unmistakable sounds of blood being spilled on the field of battle.

  The one who they call Frank made a wonderful-smelling stew from some meat that
he found, and while I couldn't really eat it, I felt it would be quite pleasant
for the group. Astonishingly, there was a lot of question about the stew itself,
to the point where I felt Frank may even be offended by the group's refusal to
sustain itself.

## Chapter 7
